'use strict';

const debug = require('winston');

const faceDetection = require('../../lib/face_detection');

const clientSet = new Set();

const onConnection = function(client) {
  clientSet.add(client.id);
  debug.info(`Socket connection: ${client.id}. Clients counter: ${clientSet.size}`);

  client.on('disconnect', () => {
    debug.info(`Socket disconnect: ${client.id}. Clients counter: ${clientSet.size}`);
    clientSet.delete(client.id);

    if (!clientSet.size) {
      faceDetection.stop();
    }
  });

  if (clientSet.size === 1) {
    faceDetection.start((err, data) => {
      if (err) {
        debug.error(err);
        return;
      }

      this.emit('frame', { buffer: data.buffer });
    });
  }
};

const init = function(socket) {
  socket.on('connection', onConnection);
};

module.exports = init;
