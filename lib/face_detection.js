'use strict';

const cv = require('opencv');
const debug = require('winston');

const CAM_WIDTH = 320;
const CAM_HEIGHT = 240;
const CAM_FPS = 5;
const CAM_INTERVAL = 1000 / CAM_FPS;
const RECT_COLOR = [0, 255, 0];
const RECT_THICKNESS = 2;
let isEnabled = false;

// initialize camera
const camera = new cv.VideoCapture(0);
camera.setWidth(CAM_WIDTH);
camera.setHeight(CAM_HEIGHT);

const recognize = function(cb) {
  debug.info(`callback call: ${Date.now()}`);
  console.time('recognize');
  console.time('camRead');
  camera.read((err, im) => {
    debug.info(`cam readed: ${Date.now()}`);
    console.timeEnd('camRead');
    if (err) {
      cb(err);
      return;
    }

    console.time('camDetect');
    im.detectObject('./node_modules/opencv/data/haarcascade_frontalface_alt2.xml', {}, (detectErr, faces) => {
      console.timeEnd('camDetect');
      if (detectErr) {
        cb(detectErr);
        return;
      }

      const arrLen = faces.length;
      for (let i = 0; i < arrLen; i++) {
        const face = faces[i];
        im.rectangle([face.x, face.y], [face.width, face.height], RECT_COLOR, RECT_THICKNESS);
      }

      console.timeEnd('recognize');
      cb(null, { buffer: im.toBuffer() });

      if (isEnabled) {
        setTimeout(recognize.bind(this, cb), CAM_INTERVAL);
      }
    });
  });
};

const start = function(cb) {
  debug.info('Start face detection');
  isEnabled = true;
  recognize(cb);
};

const stop = function() {
  debug.info('Stop face detection');
  isEnabled = false;
};

module.exports = { start, stop };
