'use strict';
/* global io */

const CANVAS_ID = 'canvas-video';

const socket = io.connect();

const canvas = document.getElementById(CANVAS_ID);
const context = canvas.getContext('2d');
const img = new Image();

// show loading notice
context.fillStyle = '#333';
context.fillText('Loading...', (canvas.width / 2) - 30, canvas.height / 3);

socket.on('frame', (data) => {
  // Reference: http://stackoverflow.com/questions/24107378/socket-io-began-to-support-binary-stream-from-1-0-is-there-a-complete-example-e/24124966#24124966
  const uint8Arr = new Uint8Array(data.buffer);
  const str = String.fromCharCode.apply(null, uint8Arr);
  const base64String = btoa(str);

  img.onload = function() {
    context.drawImage(this, 0, 0, canvas.width, canvas.height);
  };
  img.src = `data:image/png;base64,${base64String}`;
});
