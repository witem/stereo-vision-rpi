'use strict';

const http = require('http');
const path = require('path');
const debug = require('winston');
const config = require('config');
const express = require('express');

const routes = require('./routes');
const publicPath = path.resolve(__dirname, config.staticFolder);
// ===============
// ===== App =====
// ===============
const app = express();
app.set('port', config.httpPort);
app.use(express.static(publicPath));
app.get('*', (req, res) => {
  res.sendFile('index.html', { root: publicPath });
});

// ==================
// ===== Server =====
// ==================
const server = http.createServer(app);
server.listen(app.get('port'), () => {
  debug.info(`HTTP server listening on port ${app.get('port')}`);
});

// ==================
// ===== Socket =====
// ==================
const io = require('socket.io')(server);
routes.socketInit(io);

exports.app = app;
